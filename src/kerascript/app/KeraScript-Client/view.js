let cnv;
let model;
let c;
let randomBatch;
let matrix = randomBatchInput(1,100)
let size = 100;
let sqrtSize = Math.sqrt(size);

function setup() {
	cnv = createCanvas(size, size);
	c = document.getElementById("myCanvas");
	for (let element of document.getElementsByClassName("p5Canvas")) {
		element.addEventListener("contextmenu", (e) => e.preventDefault());
	}
	matrix = randomBatchInput(1,100)
}

function nullInput(){
	matrix = nullBatchInput(1,100)
}


function draw() {
	background(220);
	if ( outputtensor != null )
		tf.browser.toPixels(outputtensor, c);
	for ( let i = 0 ; i < sqrtSize ; i++ ){
		for ( let j  = 0 ; j < sqrtSize ; j++ ){
			let val = (matrix[0][j*sqrtSize+i]*255)
			fill( -val, 0, val )
			square( sqrtSize * i, sqrtSize * j, sqrtSize)
		}
	}
}

function mousePressed() {
	column =  Math.floor(mouseY/sqrtSize) % 10
	line = Math.floor(mouseX/sqrtSize) % 10

	if ( mouseButton == LEFT )
		matrix[0][(column*sqrtSize)+line] += 0.1
	else if ( mouseButton == RIGHT )
		matrix[0][(column*sqrtSize)+line] -= 0.1
}

function mouseDragged() {
	column =  Math.floor(mouseY/sqrtSize) % 10
	line = Math.floor(mouseX/sqrtSize) % 10

	if ( mouseButton == LEFT )
		matrix[0][(column*sqrtSize)+line] += 0.1
	else if ( mouseButton == RIGHT )
		matrix[0][(column*sqrtSize)+line] -= 0.1
}