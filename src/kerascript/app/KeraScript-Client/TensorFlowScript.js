const ipport = "localhost:8000";
let outputtensor;

function randomBatchInput(latentDim, nSample){
	random = [];
	for ( let i = 0 ; i < latentDim ; i ++ ){
		random[i] = [];
		for ( let j = 0 ; j < nSample ; j ++ )
			random[i][j] = (  Math.random() - 0.5 ) * 2
	}
	return random;
}

function nullBatchInput(latentDim, nSample){
	random = [];
	for ( let i = 0 ; i < latentDim ; i ++ ){
		random[i] = [];
		for ( let j = 0 ; j < nSample ; j ++ )
			random[i][j] = 0
	}
	return random;
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

(async () => {
	const model = await tf.loadLayersModel('http://'+ipport+'/gen/model.json');
	while( true && model != null ){
		if ( matrix != null)
			outputtensor = await model.predict(tf.stack(matrix)).squeeze().squeeze().div(tf.scalar(2)).add(tf.scalar(.5));
		await sleep(2000)
	}
})();