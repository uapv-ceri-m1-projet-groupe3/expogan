const express = require('express')
const app = express()
const port = 8000

app.use(express.static("/KeraScript-Client/"));
app.get('/', (req, res) => {
	res.sendFile('index.html', {root: __dirname })
})

app.get('/view', (req, res) => {
	res.sendFile('view.js', {root: __dirname })
})

app.get('/tfscript', (req, res) => {
	res.sendFile('TensorFlowScript.js', {root: __dirname })
})

app.use('/disc', express.static('./data/tfjs_discriminator'))
app.use('/gen', express.static('./data/tfjs_generator'))

app.listen(port, () => {
	console.log(`KeraScript available on : http://localhost:${port}`)
})
