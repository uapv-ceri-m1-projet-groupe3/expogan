# ExpoGAN - Build & Run with Docker

![Presentation](https://gitlab.com/uapv-ceri-m1-projet-groupe3/expogan/-/raw/master/docs/ExpoGanArchitecture.png)

## Docker setup

* [Install docker](https://docs.docker.com/get-docker/)
* [Install docker compose](https://docs.docker.com/compose/install/)

## Run

* cd in the root of this repo
* sudo docker-compose build
* sudo docker-compose up
* Go to localhost:5000/ to have our test API app 
* Go to localhost:7000/ to visit ExpoGan
* Go to localhost:8000/ to visit LatentShip

## Related projects 

* [RecolorIA](https://gitlab.com/Raymondaud.Q/RecolorIA)
* [ML GAN Émojis](https://gitlab.com/uapv-ceri-m1-projet-groupe3/ml-gan-emojis)
* [Scrapy Mojis](https://gitlab.com/uapv-ceri-m1-projet-groupe3/ml-scrapy-mojis)
* [KeraScript](https://gitlab.com/uapv-ceri-m1-projet-groupe3/kerascript)


